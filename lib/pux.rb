#! /uer/bin/env ruby
# coding: utf-8

#
# PUX API interface library for ruby
#
#   Copyright (C) 2015 PUX Corporation. All rights reserved.
#

require 'json'
require 'httpclient'
require 'uri'
require 'stringio'
require 'pp'

# PUXが提供するAPIを統括するモジュール
module PUX

    # 座標を表すクラス. 顔検出の結果で使用されます.
    # 左上座標系です.
    class Point
        def initialize( x, y)
            @x = x
            @y = y
        end

        def == (v)
            case v
            when Point
                @x == v.x && @y== v.y
                
            when Array
                @x == v[0] && @y == v[1]

            when Hash
                @x == v[:x] && @y == v[:y]

            else
                raise( "class #{x.class}'s instance is not supported.\n")
            end
        end

        # X座標を返します.
        # @return [Integer] x X座標の値.
        attr_reader :x

        # Y座標を返します.
        # @return [Integer] y X座標の値.
        attr_reader :y
    end

    # 顔検出を行った際に返される一人分のデータを抽象化したクラスです.
    # 実際の実装は各サブ機能毎に記述されます.
    class Person
        # @private
        def initialize()
            @left      = nil
            @top       = nil
            @right     = nil
            @bottom    = nil
            @left_eye  = nil
            @right_eye = nil
            @accuracy  = nil
        end

        # @return [Integer] 顔として検出した矩形の左端位置を返します.
        attr_reader :left

        # @return [Integer] 顔として検出した矩形の上端位置を返します.
        attr_reader :top

        # @return [Integer] 顔として検出した矩形の左端位置を返します.
        attr_reader :right

        # @return [Integer] 顔として検出した矩形の右端位置を返します.
        attr_reader :bottom

        # @return [Point] 左目の中心座標を返します.
        attr_reader :left_eye

        # @return [Point] 右目の中心座標を返します.
        attr_reader :right_eye

        # @return [Float] 検出の確度を0.0〜1.0の範囲で返します.
        attr_reader :accuracy
    end
end
