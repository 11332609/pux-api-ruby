#! /uer/bin/env ruby
# coding: utf-8

#
# PUX API interface library for ruby
#
#   Copyright (C) 2015 PUX Corporation. All rights reserved.
#

require 'pux'
require 'json'
require 'uri'

module PUX

    # @private
    def Point::from_hash( data)
        Point.new( data[:x], data[:y])
    end

    # PUXが提供するWebAPIを統括するモジュールです.
    # 
    # @see http://docs.polestars.jp/WebAPI/index 
    # 
    module WebAPI

        # @private
        @@agent_name      = "PUX WebAPI library for Ruby/0.1"

        if not defined?(@@service_url)
            # @private
            @@service_url = "http://eval.api.polestars.jp:8080/webapi"
        end

        # @private
        @@default_api_key = nil

        # PUX WebAPI使用時に使用するデフォルトAPIキーを設定します.
        # 個々のAPI呼び出し時にAPIキーを省略した場合、このメソッドで設定した
        # APIキーが使用されます.
        #
        # @param [String] key 登録時に付与されたAPIキーの値を指定します.
        #
        def WebAPI::default_api_key= (key)
            raise( "Illeagal format.") if /^[0-9a-f]{32}$/ !~ key
            @@default_api_key = key
        end

        # @private
        def WebAPI::default_api_key
            @@default_api_key
        end

        # @private
        def WebAPI::service_url( api_name)
            URI.parse( @@service_url + "/" + api_name)
        end

        # PUX WebAPI使用時にサーバに通知するエージェント名を設定します.
        #
        # @param [String] name エージェント名に設定する文字列を指定します.
        #
        def WebAPI::agent_name= (name)
            @@agent_name = name
        end

        # @private
        def WebAPI::agent_name
            @@agent_name
        end
    end
end
