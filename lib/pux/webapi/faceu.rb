#! /uer/bin/env ruby
# coding: utf-8

#
# PUX API interface library for ruby
#
#   Copyright (C) 2015 PUX Corporation. All rights reserved.
#

require 'pux'
require 'pux/webapi'
require 'uri'
require 'stringio'

module PUX
    module WebAPI

        #
        # PUX::Personクラスに対するPUX::WebAPI::FaceUでの顔検出使用時の実装です.
        # いくつかのメソッドを上書きします.
        #
        # @note ユーザがこのモジュールの取り込みを行う必要はありません.
        #
        module PersonForFaceDetect
            # @private
            PARTS_KEY_TABLE = {
                :boundingRectangleLeftUpper  => :BOUNDING_RECTANGLE_LEFT_UPPER,
                :boundingRectangleRightUpper => :BOUNDING_RECTANGLE_RIGHT_UPPER,
                :boundingRectangleRightLower => :BOUNDING_RECTANGLE_RIGHT_LOWER,
                :boundingRectangleLeftLower  => :BOUNDING_RECTANGLE_LEFT_LOWER,
                :leftBlackEyeCenter          => :LEFT_BLACK_EYE_CENTER,
                :rightBlackEyeCenter         => :RIGHT_BLACK_EYE_CENTER,
                :leftCheek1                  => :LEFT_CHEEK1,
                :leftCheek2                  => :LEFT_CHEEK2,
                :leftCheek3                  => :LEFT_CHEEK3,
                :leftCheek4                  => :LEFT_CHEEK4,
                :leftCheek5                  => :LEFT_CHEEK5,
                :leftCheek6                  => :LEFT_CHEEK6,
                :leftCheek7                  => :LEFT_CHEEK7,
                :chin                        => :CHIN,
                :rightCheek7                 => :RIGHT_CHEEK7,
                :rightCheek6                 => :RIGHT_CHEEK6,
                :rightCheek5                 => :RIGHT_CHEEK5,
                :rightCheek4                 => :RIGHT_CHEEK4,
                :rightCheek3                 => :RIGHT_CHEEK3,
                :rightCheek2                 => :RIGHT_CHEEK2,
                :rightCheek1                 => :RIGHT_CHEEK1,
                :parietal                    => :PARIETAL,
                :rightEyebrowOutsideEnd      => :RIGHT_EYEBROW_OUTSIDE_END,
                :rightEyebrowUpperOutside    => :RIGHT_EYEBROW_UPPER_OUTSIDE,
                :rightEyebrowUpperInside     => :RIGHT_EYEBROW_UPPER_INSIDE,
                :rightEyebrowInsideEnd       => :RIGHT_EYEBROW_INSIDE_END,
                :rightEyebrowLowerInside     => :RIGHT_EYEBROW_LOWER_INSIDE,
                :rightEyebrowLowerOutside    => :RIGHT_EYEBROW_LOWER_OUTSIDE,
                :leftEyebrowOutsideEnd       => :LEFT_EYEBROW_OUTSIDE_END,
                :leftEyebrowUpperOutside     => :LEFT_EYEBROW_UPPER_OUTSIDE,
                :leftEyebrowUpperInside      => :LEFT_EYEBROW_UPPER_INSIDE,
                :leftEyebrowInsideEnd        => :LEFT_EYEBROW_INSIDE_END,
                :leftEyebrowLowerInside      => :LEFT_EYEBROW_LOWER_INSIDE,
                :leftEyebrowLowerOutside     => :LEFT_EYEBROW_LOWER_OUTSIDE,
                :leftEyeOutsideEnd           => :LEFT_EYE_OUTSIDE_END,
                :leftEyeUpperOutside         => :LEFT_EYE_UPPER_OUTSIDE,
                :leftEyeUpperCenter          => :LEFT_EYE_UPPER_CENTER,
                :leftEyeUpperInside          => :LEFT_EYE_UPPER_INSIDE,
                :leftEyeInsideEnd            => :LEFT_EYE_INSIDE_END,
                :leftEyeLowerInside          => :LEFT_EYE_LOWER_INSIDE,
                :leftEyeLowerCenter          => :LEFT_EYE_LOWER_CENTER,
                :leftEyeLowerOutside         => :LEFT_EYE_LOWER_OUTSIDE,
                :rightEyeOutsideEnd          => :RIGHT_EYE_OUTSIDE_END,
                :rightEyeUpperOutside        => :RIGHT_EYE_UPPER_OUTSIDE,
                :rightEyeUpperCenter         => :RIGHT_EYE_UPPER_CENTER,
                :rightEyeUpperInside         => :RIGHT_EYE_UPPER_INSIDE,
                :rightEyeInsideEnd           => :RIGHT_EYE_INSIDE_END,
                :rightEyeLowerInside         => :RIGHT_EYE_LOWER_INSIDE,
                :rightEyeLowerCenter         => :RIGHT_EYE_LOWER_CENTER,
                :rightEyeLowerOutside        => :RIGHT_EYE_LOWER_OUTSIDE,
                :noseLeftLineUpper           => :NOSE_LEFT_LINE_UPPER,
                :noseLeftLineCenter          => :NOSE_LEFT_LINE_CENTER,
                :noseLeftLineLower0          => :NOSE_LEFT_LINE_LOWER0,
                :noseLeftLineLower1          => :NOSE_LEFT_LINE_LOWER1,
                :noseBottomCenter            => :NOSE_BOTTOM_CENTER,
                :noseRightLineLower1         => :NOSE_RIGHT_LINE_LOWER1,
                :noseRightLineLower0         => :NOSE_RIGHT_LINE_LOWER0,
                :noseRightLineCenter         => :NOSE_RIGHT_LINE_CENTER,
                :noseRightLineUpper          => :NOSE_RIGHT_LINE_UPPER,
                :nostrilsLeft                => :NOSTRILS_LEFT,
                :nostrilsRight               => :NOSTRILS_RIGHT,
                :noseTip                     => :NOSE_TIP,
                :noseCenterLine0             => :NOSE_CENTER_LINE0,
                :noseCenterLine1             => :NOSE_CENTER_LINE1,
                :mouthLeftEnd                => :MOUTH_LEFT_END,
                :upperLipUpperLeftOutside    => :UPPER_LIP_UPPER_LEFT_OUTSIDE,
                :upperLipUpperLeftInside     => :UPPER_LIP_UPPER_LEFT_INSIDE,
                :mouthUpperPart              => :MOUTH_UPPER_PART,
                :upperLipUpperRightInside    => :UPPER_LIP_UPPER_RIGHT_INSIDE,
                :upperLipUpperRightOutside   => :UPPER_LIP_UPPER_RIGHT_OUTSIDE,
                :mouthRightEnd               => :MOUTH_RIGHT_END,
                :lowerLipLowerRightOutside   => :LOWER_LIP_LOWER_RIGHT_OUTSIDE,
                :lowerLipLowerRightInside    => :LOWER_LIP_LOWER_RIGHT_INSIDE,
                :mouthLowerPart              => :MOUTH_LOWER_PART,
                :lowerLipLowerLeftInside     => :LOWER_LIP_LOWER_LEFT_INSIDE,
                :lowerLipLowerLeftOutside    => :LOWER_LIP_LOWER_LEFT_OUTSIDE,
                :upperLipLowerLeft           => :UPPER_LIP_LOWER_LEFT,
                :upperLipLowerCenter         => :UPPER_LIP_LOWER_CENTER,
                :upperLipLowerRight          => :UPPER_LIP_LOWER_RIGHT,
                :lowerLipUpperRight          => :LOWER_LIP_UPPER_RIGHT,
                :lowerLipUpperCenter         => :LOWER_LIP_UPPER_CENTER,
                :lowerLipUpperLeft           => :LOWER_LIP_UPPER_LEFT,
                :mouthCenter                 => :MOUTH_CENTER,
            }

            # @private
            ATTR_BLINK  = Struct.new( :left, :right)

            # @private
            ATTR_AGE    = Struct.new( :value, :accuracy)

            # @private
            ATTR_GENDER = Struct.new( :value, :accuracy)

            # @private
            ATTR_ANGLE  = Struct.new( :yaw, :pitch, :roll)

            def append_attr_blink( dat)
                dat   = dat[:blinkLevel]
                left  = dat[:leftEye] / 65535.0
                right = dat[:rightEye] / 65535.0

                @attribute.store( :BLINK, ATTR_BLINK.new( left, right))
            end
            private :append_attr_blink

            def append_attr_age( dat)
                score = dat[:ageScore] / 1000.0
                @attribute.store( :AGE, ATTR_AGE.new( dat[:ageResult], score))
            end
            private :append_attr_age

            def append_attr_gender( dat)
                case dat[:genderResult]
                when 0
                    gender = :MALE
                when 1
                    gender = :FEMALE
                else
                    gender = :UNKNOWN
                end

                score = dat[:genderScore] / 1000.0

                @attribute.store( :GENDER, ATTR_GENDER.new( gender, score))
            end
            private :append_attr_gender

            def append_attr_smile( dat)
                score = dat[:smileLevel] / 100.0

                @attribute.store( :SMILE, score)
            end
            private :append_attr_smile

            def append_attr_angle( dat)
                @attribute.store( :ANGLE,
                        ATTR_ANGLE.new( dat[:yaw], dat[:pitch], dat[:roll]))
            end
            private :append_attr_angle

            def append_attr_funny_option( dat)
                @attribute.store( :ANIMALIZE, dat[:similarAnimal])
                @attribute.store( :SMUG, dat[:doyaLevel] / 100.0)
                @attribute.store( :TROUBLED, dat[:troubleLevel] / 100.0)
            end
            private :append_attr_funny_option

            # @private
            def import( data)
                face       = data[:faceCoordinates]
                @accuracy  = face[:faceConfidence] / 1000.0
                @left      = face[:faceFrameLeftX]
                @top       = face[:faceFrameTopY]
                @right     = face[:faceFrameRightX]
                @bottom    = face[:faceFrameBottomY]
                @left_eye  = Point.new( face[:leftEyeX], face[:leftEyeY])
                @right_eye = Point.new( face[:rightEyeX], face[:rightEyeY])
                @attribute = {}
                @tags      = {}

                if data.include?( :registrationFaceInfo)
                    info     = data[:registrationFaceInfo]
                    @face_id = info[:faceId]

                    if info[:meta].include?(:tag)
                        info[:meta][:tag].each { |tag|
                            @tags.store( tag[:key].to_sym, tag[:value])
                        }
                    end

                    if info.include?( :imagePath)
                        @registered_image = info[:imagePath]
                    end
                end

                if data.include?( :facePartsCoordinates)
                    @parts = {}
                    parts  = data[:facePartsCoordinates]

                    parts.each_pair { |key, val|
                        key = PARTS_KEY_TABLE[key]
                        @parts[key] = Point.from_hash( val) if not key.nil?
                    }

                else
                    @parts = nil
                end

                if data.has_key?( :blinkJudge)
                    append_attr_blink( data[:blinkJudge])
                end

                if data.has_key?( :ageJudge)
                    append_attr_age( data[:ageJudge])
                end

                if data.has_key?( :genderJudge)
                    append_attr_gender( data[:genderJudge])
                end

                if data.has_key?( :smileJudge)
                    append_attr_smile( data[:smileJudge])
                end

                if data.has_key?( :faceAngleJudge)
                    append_attr_angle( data[:faceAngleJudge])
                end

                if data.has_key?( :enjoyJudge)
                    append_attr_funny_option( data[:enjoyJudge])
                end

                self
            end

            #
            # 顔認証用画像を登録した画像のURLを返します
            #
            # @return [String] 顔画像のURL
            #
            def registered_image
                @registered_image
            end

            #
            # 顔パーツの座標を返します(オプション).
            # 検出時に :parts_detectオプションをtrueに設定した場合のみ使用可能
            # です.
            #
            # @return [Hash] 各パーツの座標が格納されたHashを返します.
            #
            def parts
                @parts
            end

            #
            # 性別を返します.
            # 検出時に:ageオプションまたは:genderオプションをtrueに設定した場合
            # のみ使用可能です.
            #
            # @return [Symbol] :MALE,:FEMALE,:UNKNOWNのいずれかが返されます
            #
            def gender
                tmp = @attribute[:GENDER]
                (not tmp.nil?)? tmp.value: nil
            end

            #
            # 性別判定を行います.
            # 検出時に:ageオプションまたは:genderオプションをtrueに設定した場合
            # のみ使用可能です.
            #
            # @return [Boolean] 性別が男性の場合にtrueが返されます.
            #
            def male?
                gender == :MALE
            end

            #
            # 性別判定を行います.
            # 検出時に :age オプションまたは :gender オプションをtrueに設定し
            # た場合のみ使用可能です.
            #
            # @return [Boolean] 性別が女性の場合にtrueが返されます.
            #
            def female?
                gender == :FEMALE
            end

            #
            # 笑顔のレベルを返します.
            # 検出時に :smile オプションをtrueに設定した場合のみ使用可能です.
            #
            # @return [Float] 笑顔のレベルを0.0〜1.0の範囲で返します.
            #
            def smile
                @attribute[ :SMILE]
            end

            #
            # 年齢を返します.
            # 検出時に :age オプションまたは :gender オプションをtrueに設定し
            # た場合のみ使用可能です.
            #
            # @return [Integer] 年齢の数値を返します.
            #
            def age
                tmp = @attribute[:AGE]
                (not tmp.nil?)? tmp.value: nil
            end

            #
            # 似た動物の名前を返します.
            # 検出時に :funny_option オプションをtrueに設定した場合のみ使用可能
            # です.
            #
            # @return [String] 動物名（英語）を返します.
            #
            def animalize
                @attribute[:ANIMALIZE]
            end

            #
            # ドヤ顔のレベルを返します.
            # 検出時に :funny_option オプションをtrueに設定した場合のみ使用可能
            # です.
            #
            # @return [Float] ドヤ顔の強さを0.0〜1.0の範囲で返します.
            #
            def smug
                @attribute[:SMUG]
            end

            #
            # 困り顔のレベルを返します.
            # 検出時に :funny_option オプションをtrueに設定した場合のみ使用可能
            # です.
            #
            # @return [Float] 困り顔の強さを0.0〜1.0の範囲で返します.
            #
            def troubled
                @attribute[:TROUBLED]
            end

            #
            # 画像登録時のfaceIdを返します
            #
            # @return (Interger)
            #    faceIdを返します。　
            #
            def face_id
                @face_id
            end

            #
            # 指定されたキーに対応するタグが設定されているか検査します。
            #
            # @param (Symbol) key
            #    タグのキーを指定します
            #
            # @return (Boolean)
            #    指定されてキーに対応するタグが設定されている場合はtrueを
            #    設定されていない場合はfalseを返します。
            #
            def has?( key)
                @tags.has_key?( key)
            end

            #
            # 画像登録時のタグの値を取得します.
            #
            # @param (Symbol) key
            #    タグのキーを指定します.
            #
            # @return (String)
            #    タグの値を返します.
            #    キーに対応するタグが設定されていない場合はnilを返します.
            #
            def [] (key)
                @tags[key]
            end
        end

        #
        # 識別対象を特定する為の情報を抽象化したクラスです.
        #
        class WhoInfo

            # @private
            def initialize( data)
                @face_id  = data[:faceId]
                @accuracy = data[:score] / 100.0 if data.include?(:score)
                @tags     = {}
                meta      = data[:meta]

                if meta.include?(:tag)
                    meta[:tag].each { |tag|
                        @tags.store( tag[:key].to_sym, tag[:value])
                    }
                end

                if data.include?( :imagePath)
                    @registered_image = data[:imagePath]
                end
            end

            #
            # faceIdを返します.
            #
            # @return [Integer] face_id
            #
            attr_reader :face_id

            #
            # 識別の確度を返します(オプション).
            # 人物識別実行時のみ使用できます.
            #
            # @return [Float] accuracy 識別の確度を0.0〜1.0の範囲で返します.
            #
            attr_reader :accuracy

            #
            # 登録画像を返します(オプション).
            # 登録情報の取得実行時のみ使用できます.
            #
            # @return [String] accuracy 登録画像へのURLを返します.
            #
            attr_reader :registered_image

            #
            # タグの値を取得します.
            #
            # @param (Symbol) key
            #    タグのキーを指定します.
            #
            # @return (String)
            #    タグの値を返します.
            #    キーに対応するタグが設定されていない場合はnilを返します.
            #
            def [] (key)
                @tags[key]
            end

            #
            # 指定されたキーに対応するタグが設定されているか検査します.
            #
            # @param (Symbol) key
            #    タグのキーを指定します
            #
            # @return (Boolean)
            #    指定されてキーに対応するタグが設定されている場合はtrueを
            #    設定されていない場合はfalseを返します。
            #
            def has?(key)
                @tags.has_key?( key)
            end
        end


        #
        # PUX::Personクラスに対するPUX::WebAPI::FaceUでの顔識別用時の実装です.
        # いくつかのメソッドを上書きします.
        #
        # @note ユーザがこのモジュールの取り込みを行う必要はありません.
        #
        module PersonForFaceIdentify
            # @private
            def import( data)
                face        = data[:faceCoordinates]
                @accuracy   = face[:faceConfidence] / 1000.0
                @left       = face[:faceFrameLeftX]
                @top        = face[:faceFrameTopY]
                @right      = face[:faceFrameRightX]
                @bottom     = face[:faceFrameBottomY]
                @left_eye   = Point.new( face[:leftEyeX], face[:leftEyeY])
                @right_eye  = Point.new( face[:rightEyeX], face[:rightEyeY])
                @candidates = []

                data[:candidate].each { |who|
                    @candidates << WhoInfo.new( who)
                }
            end

            #
            # 第一候補のfaceIdを返します
            #
            # @return (Interger)
            #    faceIdを返します。　
            #
            def face_id
                @candidates.first.face_id
            end

            #
            # 認識の確度を返します.
            #
            # @param (symbol) type
            #   確度の種別を指定します.
            #   * :IDENTIFYを指定した場合は識別の確度を返します.
            #   * :DETECTを指定した場合は当該矩形の人物検出の確度を返します.
            #   * :TOTALを指定した場合は総合的な確度を返します.
            #
            # @return (Float) 指定された種別に対応する確度を返します。
            #
            def accuracy( type = :IDENTIFY)
                case type
                when :IDENTIFY
                    @candidates.first.accuracy

                when :DETECT
                    @accuracy

                when :TOTAL
                    @candidates.first.accuracy * @accuracy

                else
                    raise( "Unknwon accuracy type specified.")
                end
            end

            #
            # 第一候補に対して、指定されたキーに対応するタグが設定されているか
            # 検査します。
            #
            # @param (Symbol) key
            #    タグのキーを指定します
            #
            # @return (Boolean)
            #    指定されてキーに対応するタグが設定されている場合はtrueを
            #    設定されていない場合はfalseを返します。
            #
            def has?( key)
                @candidates.first.has?( key)
            end

            #
            # 第一候補から、タグの値を取得します.
            #
            # @param (Symbol) key
            #    タグのキーを指定します.
            #
            # @return (String)
            #    タグの値を返します.
            #    キーに対応するタグが設定されていない場合はnilを返します.
            #
            def [] (key)
                @candidates.first[key]
            end

            #
            # 識別候補のリストを返します
            #
            # @return [Array<WhoInfo>] 識別情報のリスト
            #
            def candiates
                @candidates
            end
        end

        #
        # PUXの提供するWebAPIのFaceU機能の呼び出しインタフェースクラスです.
        # WebAPIで用意される機能のうち、FaceU関連の機能(顔検出,顔認証)の機能は
        # このクラスのインスタンスを通して呼び出します.
        #
        # @example 顔検出機能の呼び出し.
        #
        #   api = PUX::WebAPI::FaceU.new( :parts_detect => true)
        #   p api.detect( URI.new( "http://test.example/face.jpg"))
        #
        #   api.parts_detect = false
        #   api.age = true
        #
        #   resp = api.detect( File.new( "sample.jpg"))
        #   resp.each { |person|
        #       p person.age
        #   }
        #
        class FaceU

            # PUXの提供するWebAPIのFaceU機能の呼び出しインタフェースをオブジェ
            # クトを生成します.
            #
            # @param [Hash] opt オプションを指定します.
            #     optにnilを指定した場合（省略した場合）は全てのオプションに対
            #     しデフォルト値を設定します.
            #
            # @option opt [String] :api_key
            #     リクエスト発行時に使用するAPIキーを指定します. 省略した場合
            #     PUX::WebAPI::default_api_keyで指定したキーが使用されます.
            #
            # @option opt [String] :face_size
            #     顔検出を行う場合の最小顔サイズを指定します. "SMALL", "MIDDLE",
            #     "LARGE"のいずれかを指定します（デフォルト値は"MIDDLE").
            #     
            # @option opt [Boolean] :parts_detect
            #     顔検出時に部品検出を行う場合はtrueを設定します.
            #     （デフォルト値はfalse)
            #
            # @option opt [Boolean] :blink
            #     顔検出時にまばたき検出を行う場合はtrueを設定します.
            #     （デフォルト値はfalse)
            #     
            # @option opt [Boolean] :gender
            #     顔検出時に性別検出を行う場合はtrueを設定します.
            #     （デフォルト値はfalse)
            #
            # @option opt [Boolean] :age
            #     顔検出時に年齢検出を行う場合はtrueを設定します.
            #     （デフォルト値はfalse)
            #
            # @option opt [Boolean] :angle
            #     顔検出時に顔向き検出を行う場合はtrueを設定します.
            #     （デフォルト値はfalse)
            #     
            # @option opt [Boolean] :smile
            #     顔検出時に笑顔判定を行う場合はtrueを設定します.
            #     （デフォルト値はfalse)
            #     
            # @option opt [Boolean] :funny_option
            #     顔検出時におまけ機能を使用する場合はtrueを指定します.
            #     （デフォルト値はfalse)
            #     
            # @return [void]
            #     
            def initialize( opt = nil)
                case opt
                when nil
                    self.api_key      = WebAPI::default_api_key
                    self.face_size    = 2
                    self.parts_detect = false
                    self.blink        = false
                    self.gender       = false
                    self.age          = false
                    self.angle        = false
                    self.smile        = false
                    self.funny_option = false

                when Hash
                    self.api_key      = opt[:api_key]
                    self.face_size    = opt[:face_size]
                    self.parts_detect = opt[:parts_detect]
                    self.blink        = opt[:blink]
                    self.gender       = opt[:gender]
                    self.age          = opt[:age]
                    self.angle        = opt[:angle]
                    self.smile        = opt[:smile]
                    self.funny_option = opt[:funny_option]

                else
                    raise( "Illeagal option object")
                end
            end

            # @private
            FACE_SIZE_TABLE = {
                "SMALL"  => 1,
                "MIDDLE" => 2,
                "LARGE"  => 3,
            }

            #
            # api_keyを変更します.
            #
            # @param [String] val
            #     リクエストを発行する際に使用するAPIキーを指定します.
            #
            # @return [void]
            #
            def api_key= (val)
                case val
                when String
                    raise( "Illeagal api_key value") if /^[0-9a-f]{32}$/ !~ val
                    @api_key = val
                
                when nil, :DEFAULT
                    @api_key = WebAPI::default_api_key

                else
                    raise( "Illeagal api_key value")
                end
            end

            #
            # 顔検出時の最小顔サイズを変更します.
            #
            # @overload face_size= (size)
            #   サイズを文字列で指定します.
            #   @param [String] size "SMALL", "MIDDLE", "LARGE"のいずれかを指定.
            #
            # @overload face_size= (size)
            #   サイズを数字で指定します.
            #   @param [Integer] size 1(小), 2(中), 3(大)のいずれかを指定.
            #
            # @overload face_size= (size)
            #   最小顔サイズをデフォルト値にリセットします.
            #   @param [nil] size
            #
            # @return [void]
            #
            def face_size= (val)
                case val
                when String
                    @face_size = FACE_SIZE_TABLE[ val.upcase]

                when nil
                    @face_size = 2

                when 1,2,3
                    @face_size = val
                end

                raise( "Illeagal face_size value") if @face_size.nil?
            end

            def to_01( f)
                (f)? "1": "0"
            end
            private :to_01

            #
            # パーツ検出オプションの設定を変更します.
            #
            # @param [Boolean] val
            #   パーツ検出機能を使用する場合trueを設定します.
            #   使用しない場合はfalseを設定します
            #
            # @return [void]
            #
            def parts_detect= (val)
                @parts_detect = !!val
            end

            #
            # まばたき検出オプションの設定を変更します.
            #
            # @param [Boolean] val
            #   まばたき検出機能を使用する場合trueを設定します.
            #   使用しない場合はfalseを設定します
            #
            # @return [void]
            #
            def blink= (val)
                @blink = !!val
            end

            #
            # 性別検出オプションの設定を変更します
            #
            # @param [Boolean] val
            #   性別検出機能を使用する場合trueを設定します.
            #   使用しない場合はfalseを設定します
            #
            # @return [void]
            #
            def gender= (val)
                @gender = !!val
            end

            #
            # 年齢検出オプションの設定を変更します
            #
            # @param [Boolean] val
            #   年齢検出機能を使用する場合trueを設定します.
            #   使用しない場合はfalseを設定します
            #
            # @return [void]
            #
            def age= (val)
                @age = !!val
            end

            #
            # 顔向き角度検出オプションの設定を変更します
            #
            # @param [Boolean] val
            #   顔向き確度検出機能を使用する場合trueを設定します.
            #   使用しない場合はfalseを設定します
            #
            # @return [void]
            #
            def angle= (val)
                @angle = !!val
            end

            #
            # 笑顔検出オプションの設定を変更します
            #
            # @param [Boolean] val
            #   笑顔検出機能を使用する場合trueを設定します.
            #   使用しない場合はfalseを設定します
            #
            # @return [void]
            #
            def smile= (val)
                @smile = !!val
            end

            #
            # お楽しみ機能オプションの設定を変更します
            #
            # @param [Boolean] val
            #   お楽しみ機能を使用する場合trueを設定します.
            #   使用しない場合はfalseを設定します
            #
            # @return [void]
            #
            def funny_option= (val)
                @funny_option = !!val
            end

            def strip_response( resp)
                JSON.parse( resp,
                        :symbolize_names => true)[:results][:faceRecognition]
            end
            private :strip_response

            def inject( method, body)
                url    = WebAPI::service_url( "face.do")
                client = HTTPClient.new( :agent_name => WebAPI::agent_name)

                body << ["apiKey", @api_key]
                body << ["response", "json"]

                begin
                    case method
                    when :POST
                        resp = client.post_content( url, body)

                    when :GET
                        resp = client.get_content( url, body)
                    end

                rescue => e
                    raise( "%03d %s:%s" %
                                [e.res.status, e.res.reason, e.res.body])
                end

                strip_response( resp)
            end
            private :inject

            def create_common_body
                ret = []

                ret << ["optionFlgMinFaceWidth", @face_size]

                ret << ["facePartsCoordinates", "0"] if not @parts_detect
                ret << ["blinkJudge", "0"] if not @blink
                ret << ["ageAndgenderJudge", "0"] if not @age || @gender
                ret << ["angleJudge", "0"] if not @angle
                ret << ["smileJudge", "0"] if not @smile
                ret << ["enjoyJudge", "1"] if @funny_option

                ret
            end
            private :create_common_body

            def parse_face_detect_result( result)
                ret    = []

                if result.has_key?( :detectionFaceInfo)
                    result[:detectionFaceInfo].each { |info|
                        person = Person.new
                        person.extend( PersonForFaceDetect)
                        person.import( info)
        
                        ret << person
                    }
                end

                ret
            end
            private :parse_face_detect_result

            def append_tag_info( body, name, tags)
                tags.each_pair { |key,val|
                    data = (val.nil?)? key.to_s: "#{key}:#{val}"
                    body << [name, data]
                }
            end
            private :append_tag_info

            def parse_face_identify_result( result)
                ret    = []

                if result.has_key?( :verificationFaceInfo)
                    result[:verificationFaceInfo].each { |info|
                        next if not info.has_key?( :candidate)

                        person = Person.new
                        person.extend( PersonForFaceIdentify)
                        person.import( info)
        
                        ret << person
                    }
                end

                ret
            end
            private :parse_face_identify_result

            #
            # 顔検出機能の呼び出しを行います.
            # 検出対象の画像を引数で指定します.
            #
            # @overload detect( file)
            #   ローカルファイルの画像が指定された物として扱います.
            #   @param [IO] file
            #      ローカルファイルのIOオブジェクトを指定します.
            #
            # @overload detect( url)
            #   外部の画像が指定された物として扱います.
            #   @param [URI] url 
            #      外部のサーバ上の画像に対するURLを指定します.
            #
            # @overload detect( data, content_type)
            #   ローカルデータが渡された物として扱います（未実装）.
            #   @param [String] data 
            #      ローカルデータそのものを指定します.
            #   @param [String] content_type 
            #      データに対するContentTypeを指定します.
            #
            # @return [Array<PUX::Person>] 
            #   検出結果を格納した配列を返します.
            #   画像中から人物を検出できなかった場合は、長さ0の配列が返されます.
            #
            def detect( arg)
                body = create_common_body()

                case arg
                when IO
                    begin
                        body << ["inputFile", arg]
                        result = inject( :POST, body)
                    ensure
                        arg.rewind
                    end

                when URI
                    body << ["imageURL", arg.to_s]
                    result = inject( :GET, body)

                else
                    raise( "Unsupported class \"%s\" instance." % arg.class)
                end

                parse_face_detect_result( result)
            end

            #
            # 認証用顔登録機能の呼び出しを行います.
            # 登録する画像を引数で指定し、画像中で顔認識で検出された画像を認識
            # 対象として登録します。
            #
            # @overload register( file, tags = nil)
            #   ローカルファイルの画像が指定された物として扱います.
            #   @param [IO] file
            #      ローカルファイルのIOオブジェクトを指定します.
            #   @param [Hash<Symbol,String>] tags
            #      識別画像に対するタグ情報(省略可能)
            #
            # @overload register( url, tags = nil)
            #   外部の画像が指定された物として扱います.
            #   @param [URI] url 
            #      外部のサーバ上の画像に対するURLを指定します.
            #   @param [Hash<Symbol,String>] tag
            #      識別画像に対するタグ情報(省略可能)
            #
            # @overload register( data, content_type, tags = nil)
            #   ローカルデータが渡された物として扱います（未実装）.
            #   @param [String] data 
            #      ローカルデータそのものを指定します.
            #   @param [String] content_type 
            #      データに対するContentTypeを指定します.
            #   @param [Hash<Symbol,String>] tags
            #      識別画像に対するタグ情報(省略可能)
            #
            # @return [Array<PUX::Person>] 
            #   登録結果を格納した配列を返します.
            #   画像中から人物を検出できなかった場合は、長さ0の配列が返されます.
            #
            def register( arg, tags = nil)
                body   = create_common_body()

                body << ["mode", "register"]
                append_tag_info( body, "tag", tags) if not tags.nil?

                case arg
                when IO
                    begin
                        body << ["inputFile", arg]
                        result = inject( :POST, body)
                    ensure
                        arg.rewind
                    end

                when URI
                    body << ["imageURL", arg.to_s]
                    result = inject( :GET, body)

                else
                    raise( "Unsupported class \"%s\" instance." % arg.class)
                end

                parse_face_detect_result( result)
            end

            #
            # 認証機能の呼び出しを行います.
            # 認証する画像を引数で指定し、登録された人物が存在するか検査します
            #
            # @overload who( file, tag = nil)
            #   ローカルファイルの画像が指定された物として扱います.
            #   @param [IO] file
            #      ローカルファイルのIOオブジェクトを指定します.
            #   @param [Hash<Symbol,String>] tag
            #      識別画像に対するタグ情報(省略可能)
            #
            # @overload who( url, tag = nil)
            #   外部の画像が指定された物として扱います.
            #   @param [URI] url 
            #      外部のサーバ上の画像に対するURLを指定します.
            #   @param [Hash<Symbol,String>] tag
            #      識別画像に対するタグ情報(省略可能)
            #
            # @overload who( data, content_type, tag = nil)
            #   ローカルデータが渡された物として扱います（未実装）.
            #   @param [String] data 
            #      ローカルデータそのものを指定します.
            #   @param [String] content_type 
            #      データに対するContentTypeを指定します.
            #   @param [Hash<Symbol,String>] tag
            #      識別画像に対するタグ情報(省略可能)
            #
            # @return [Array<PUX::Person>] 
            #   登録結果を格納した配列を返します.
            #   画像中から人物を検出できなかった場合は、長さ0の配列が返されます.
            #
            def who( arg, tag = nil)
                body   = []

                body << ["mode", "verify"]

                case tag
                when Hash
                    append_tag_info( body, "tagCondition", tags)

                when Symbol
                    body << [ "tagCondition", key.to_s]

                when nil
                    # Nothing

                else
                    raise( "Unsupported tag type \"%s\"." % arg.class)
                end

                case arg
                when IO
                    begin
                        body << ["inputFile", arg]
                        result = inject( :POST, body)
                    ensure
                        arg.rewind
                    end

                when URI
                    body << ["imageURL", arg.to_s]
                    result = inject( :GET, body)

                else
                    raise( "Unsupported class \"%s\" instance." % arg.class)
                end

                parse_face_identify_result( result)
            end

            #
            # 顔情報登録解除機能の呼び出しを行います.
            #
            # @overload unregister( cond)
            #   解除する情報をタグ情報とのマッチによって指定します.
            #
            #   @param [Hash<Symbol,String>] cond
            #     削除する顔を指定する為の条件をHashで指定します.
            #
            #   @return [Array<Integer>] 
            #     削除した顔情報に対応するfaceIdの配列を返します.
            #     削除する顔情報がなかった場合は、長さ0の配列が返されます.
            #
            #   @example タグ情報にマッチさせる場合
            #     api.unregister( :group => "aliance", :name => "kuwagata")
            #
            # @overload unregister( face_id)
            #   解除する情報をfaceIdで指定します.
            #
            #   @param [Integer] face_id
            #     faceIdを指定します.
            #
            #   @return [Boolean] 
            #     顔情報を削除した場合はtrueを返します.
            #     指定されたfaceIdの顔情報がなかった場合はfalseを返します.
            #
            #   @example faceIdが12番の情報の登録を解除する場合
            #     api.unregister( 12)
            #
            # @overload unregister( key)
            #   @param [Symbol] key
            #     削除する顔をタグのキーで指定します(指定されたキーを設定され
            #     た情報全ての登録を解除します).
            #
            #   @return [Array<Integer>] 
            #     削除した顔情報に対応するfaceIdの配列を返します.
            #     削除する顔情報がなかった場合は、長さ0の配列が返されます.
            #
            # @overload unregister(nil)
            #   全情報を解除します(デフォルト).
            #
            #   @return [Boolean] 
            #     顔情報を削除した場合はtrueを返します.
            #
            def unregister( arg = nil)
                case arg
                when Hash
                    ret = unregister_with_hash( arg)

                when Integer, nil
                    ret = unregister_with_integer( arg)

                when Symbol
                    ret = unregister_with_symbol( arg)
                
                else
                    raise( "Unsupported class \"%s\" instance." % arg.class)
                end

                ret
            end

            #
            # @private
            # @note unregister()の下請け関数です
            #
            def unregister_with_hash( cond)
                body = []
                ret  = []

                body << ["mode", "delete"]
                append_tag_info( body, "tagCondition", cond)

                result = inject( :GET, body)

                if result.include?(:deleteInfo)
                    result[:deleteInfo].each { |info|
                        next if not info.include?( :faceId)

                        ret << info[:faceId]
                    }
                end

                ret
            end
            private :unregister_with_hash

            #
            # @private
            # @note unregister()の下請け関数です
            #
            def unregister_with_integer( face_id)
                body = []

                body << ["mode", "delete"]
                body << ["faceId", (not face_id.nil?)? face_id: "ALL"]

                result = inject( :GET, body)

                result[:status] == "success"
            end
            private :unregister_with_integer

            #
            # @private
            # @note unregister()の下請け関数です
            #
            def unregister_with_symbol( key)
                body = []
                ret  = []

                body << ["mode", "delete"]
                body << ["tagCondition", key.to_s]

                result = inject( :GET, body)

                if result.include?(:deleteInfo)
                    result[:deleteInfo].each { |info|
                        next if not info.include?( :faceId)

                        ret << info[:faceId]
                    }
                end

                ret
            end
            private :unregister_with_symbol

            #
            # 登録情報読み出し機能の呼び出しを行います.
            #
            # @overload list( cond)
            #   解除する情報をタグ情報とのマッチによって指定します.
            #
            #   @param [Hash<Symbol,String>] cond
            #     取得する情報を指定する為の条件をHashで指定します.
            #
            #   @example タグ情報にマッチさせる場合
            #     api.list( :group => "aliance")
            #
            #   @return [Array<WhoInfo>] 
            #     取得した情報のリストを返します
            #
            # @overload list( face_id)
            #   取得する情報をfaceIdで指定します.
            #
            #   @param [Integer] face_id
            #     faceIdを指定します.
            #
            #   @example faceIdが12番の情報を取得する場合
            #     api.list( 12)
            #
            #   @return [WhoInfo] 
            #     取得した情報を返します
            #
            # @overload list( key)
            #   解除する情報をタグ情報とのマッチによって指定します.
            #
            #   @param [Symbol] key
            #     取得する情報をキーで指定します(指定されたキーを持つ情報全て
            #     を取得します).
            #
            #   @example タグ情報にマッチさせる場合
            #     api.list( :group => "aliance")
            #
            #   @return [Array<WhoInfo>] 
            #     取得した情報のリストを返します
            #
            #
            # @overload list( nil)
            #   全情報を取得します(デフォルト).
            #
            #   @return [Array<WhoInfo>] 
            #     取得した情報のリストを返します
            #
            def list( arg = nil)
                case arg
                when Hash
                    list_with_hash( arg)

                when Integer
                    list_with_integer( arg)

                when Symbol, nil
                    list_with_symbol( arg)

                else
                    raise( "Unsupported class \"%s\" instance." % arg.class)
                end
            end

            #
            # @private
            # @note list()の下請け関数です
            #
            def list_with_hash( cond)
                body = []

                body << ["mode", "list"]
                append_tag_info( body, "tagCondition", cond)

                result = inject( :GET, body)

                if result.include?(:listInfo)
                    result[:listInfo].each { |info|
                        ret << WhoInfo.new( info)
                    }
                end

                ret
            end
            private :list_with_hash

            #
            # @private
            # @note list()の下請け関数です
            #
            def list_with_integer( face_id)
                body = []

                body << ["mode", "list"]
                body << ["faceId", face_id]

                result = inject( :GET, body)

                if result.include?(:listInfo)
                    WhoInfo.new( result[:listInfo].first)
                else
                    nil
                end
            end
            private :list_with_integer

            #
            # @private
            # @note list()の下請け関数です
            #
            def list_with_symbol( key)
                body = []
                ret  = []

                body << ["mode", "list"]
                body << ["tagCondition", key.to_s] if not key.nil?

                result = inject( :GET, body)

                if result.include?(:listInfo)
                    result[:listInfo].each { |info|
                        ret << WhoInfo.new( info)
                    }
                end

                ret
            end
            private :list_with_symbol

            #
            # タグの削除を行います.
            #
            # @overload remove_tag( cond, *keys)
            #   タグ情報にマッチングした登録情報から、指定のタグを削除します.
            #
            #   @param [Hash<Symbol,String>] cond
            #     タグ削除の対象指定条件をHashで指定します.
            #
            #   @param [Array<Symbol>] keys
            #     削除するタグのキーのリストを指定します。
            #
            #   @example タグ情報にマッチさせる場合
            #     api.remove_tag( {:name => "田中"}, :salary, :credential)
            #
            # @overload remove_tag( face_id, *keys)
            #   faceIdが一致した登録情報から、指定のタグを削除します.
            #
            #   @param [Integer] face_id
            #     タグ削除の対象指定条件をHashで指定します.
            #
            #   @param [Array<Symbol>] keys
            #     削除するタグのキーのリストを指定します。
            #
            #   @example faceIdが10番の登録情報から指定のタグを削除
            #     api.remove_tag( 10, :credential)
            #
            # @overload remove_tag( key, *keys)
            #   キーに対応するタグを持つ登録情報から、指定のタグを削除します.
            #
            #   @param [Symbol] key
            #     タグ削除の対象をタグのキーで指定します.
            #
            #   @param [Array<Symbol>] keys
            #     削除するタグのキーのリストを指定します。
            #
            #   @example キーが:outsiderのタグを持つ登録情報からタグを削除
            #     api.remove_tag( :outsider, :credential, :group)
            #
            # @overload remove_tag( nil, *keys)
            #   全ての登録情報から指定のタグを削除します.
            #
            # @return [void] 
            #
            def remove_tag( arg, *keys)
                body = []

                body << ["mode", "tag_edit"]
                body << ["method", "delete"]
                keys.each do |key| body << ["tag", key.to_s] end

                case arg
                when Hash
                    append_tag_info( body, "tagCondition", arg)

                when Integer
                    body << ["faceId", arg]

                when Symbol
                    body << ["tagCondition", arg.to_s]

                when nil
                    body << ["faceId", "ALL"]

                else
                    raise( "Unsupported class \"%s\" instance." % arg.class)
                end

                inject( :GET, body)

                nil
            end

            #
            # タグ設定を行います.
            # 既に同じキーのタグがある場合は上書きします.
            #
            # @overload put_tag( cond, tags)
            #   タグ情報にマッチングした登録情報に、指定のタグを設定します。
            #
            #   @param [Hash<Symbol,String>] cond
            #     タグ設定の対象指定条件をHashで指定します.
            #
            #   @param [Hash<Symbol,String>] tags
            #     設定するタグをHashで指定します。
            #
            #   @example タグ情報にマッチさせる場合
            #     api.put_tag( {:name => "田中"}, {:salary => 200_000})
            #
            # @overload put_tag( face_id, tags)
            #   faceIdが一致した登録情報から、指定のタグを削除します。
            #
            #   @param [Integer] face_id
            #     タグ削除の対象指定条件をHashで指定します.
            #
            #   @param [Hash<Symbol,String>] tags
            #     設定するタグをHashで指定します。
            #
            #   @example faceIdが10番の登録情報にタグを設定
            #     api.put_tag( 10, :credential => "all pass")
            #
            # @overload remove_tag( key, *keys)
            #   キーに対応するタグを持つ登録情報に、指定のタグを設定します。
            #
            #   @param [Symbol] key
            #     タグ設定の対象をタグのキーで指定します.
            #
            #   @param [Hash<Symbol,String>] tags
            #     設定するタグをHashで指定します。
            #
            #   @example キーが:outsiderのタグを持つ登録情報からタグを削除
            #     api.remove_tag( :outsider, {:credential => "do not touch"})
            #
            # @return [void] 
            #
            def put_tag( arg, tags)
                body = []

                body << ["mode", "tag_edit"]
                body << ["method", "add"]
                append_tag_info( body, "tag", tags)

                case arg
                when Hash
                    append_tag_info( body, "tagCondition", arg)

                when Integer
                    body << ["faceId", arg]

                when Symbol
                    body << ["tagCondition", arg.to_s]

                else
                    raise( "Unsupported class \"%s\" instance." % arg.class)
                end

                inject( :GET, body)

                nil
            end
        end
    end
end
