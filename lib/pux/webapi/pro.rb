#! /uer/bin/env ruby
# coding: utf-8

#
# PUX API interface library for ruby
#
#   Copyright (C) 2015 PUX Corporation. All rights reserved.
#

module PUX
    module WebAPI
        # @private
        # @notice 仮のURLです
        @@service_url = "http://pro.api.polestars.jp:8080/webapi"
    end
end
