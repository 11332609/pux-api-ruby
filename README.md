# PUX API interface library for Ruby

T.B.D  
Reference => [here](_index.html)

## 使い方
httpclientを使用していますので、以下のようにして事前にインストールしておいてください。

    % gem install httpclient


あとはlib/puxを適当なところにコピーして使ってください。
だれかgem化してください。

## テストの実行方法

RubyUnitによるテストが用意されています。以下のようにして実行してください。 

    % ruby tests/run_test.rb

## サンプル 

### 顔検出
    require 'pux/webapi/faceu'
    
    api = PUX::WebAPI::FaceU.new( :api_key => 'XXXXXXXXXXX', :age => true)
    
    resp = api.detect( File.new( sample.jpg))
    resp.each { |person|
        print( "%d\n", person.age)
    }

### 顔識別

#### 登録
    api.register( File.new( "sample1.jpg"),
		:name => '田中一郎', :TEAM_A => "下から3番目")

#### 識別
    resp = api.who( File.new( "sample2.jpg"))
    p resp.first[:name]

#### 登録解除
    resp = api.unregister( :TEAM_A)

### サンプルコード
簡単なサンプルコードがsamplesに格納されています。
`define.rb`をローカルに作成し、`TEST_API_KEY`および`TEST_IMAGE_URL`を設定して、以下のようにすれば動作を試す事ができます。

    % ruby -r./define -Ilib samples/detect_sample.rb

## TODO
そのうちヒマができたら

* ドキュメント整備
*  API追加(オブジェクト認識 etc)
* サンプル追加
* gem化
* Unitテストの整備 （作業中）
