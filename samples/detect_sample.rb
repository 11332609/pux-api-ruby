#! /usr/bin/env ruby22
# coding: utf-8

#require 'pux/webapi/pro.rb'
require 'pux/webapi/faceu.rb'
require 'pp'

if not defined?(TEST_API_KEY)
    raise( "Please set your api key to constatnt TEST_API_KEY.")
end

if not defined?(TEST_IMAGE_URL)
    raise( "Please set test image's URL to constatnt TEST_IMAGE_URL.")
end

PUX::WebAPI.default_api_key = TEST_API_KEY

opt = {
    :face_size    => "SMALL",
    :age          => true,
    :smile        => true,
    :funny_option => true
}
api = PUX::WebAPI::FaceU.new( opt)

print "**** PUX::WebAPI::FaceU#detect( URI)\n"
pp api.detect( URI.parse( TEST_IMAGE_URL))
printf( "\n")

api.parts_detect = true
api.age          = true
api.gender       = true
api.angle        = true
api.smile        = true
api.blink        = true
api.funny_option = true

print "**** PUX::WebAPI::FaceU#detect( IO)\n"
pp api.detect( File.new( "sample.jpg"))

