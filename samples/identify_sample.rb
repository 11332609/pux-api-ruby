#! /usr/bin/env ruby22
# coding: utf-8

if not defined?(TEST_API_KEY)
    raise( "Please set your api key to constant TEST_API_KEY.")
end

if not defined?(TEST_IMAGE_URL)
    raise( "Please set test image's URL to constant TEST_IMAGE_URL")
end

require 'pux/webapi/faceu.rb'
require 'pp'

PUX::WebAPI.default_api_key = TEST_API_KEY

api = PUX::WebAPI::FaceU.new( :face_size => "SMALL")

print "**** PUX::WebAPI::FaceU#register\n"
pp api.register( URI.parse( TEST_IMAGE_URL), :group => "Aチーム")

print "**** PUX::WebAPI::FaceU#who\n"
pp api.who( URI.parse( TEST_IMAGE_URL))

print "**** PUX::WebAPI::FaceU#unregister(Integer)\n"
pp api.unregister( 7)

print "**** PUX::WebAPI::FaceU#list(Integer)\n"
pp api.list( 1)

print "**** PUX::WebAPI::FaceU#put_tag( Symbol, Hash<Symbol,String>)\n"
pp api.put_tag( :group, :salary => 1000_000)
pp api.list

print "**** PUX::WebAPI::FaceU#remove_tag( Hash<Symbol,String>, Symbol)\n"
pp api.remove_tag( {:salary => 20000}, :group)
pp api.list

print "**** PUX::WebAPI::FaceU#unregister( Symbol)\n"
pp api.unregister( :group)

print "**** PUX::WebAPI::FaceU#unregister( nil)\n"
pp api.unregister()
