#! /usr/bin/env/ruby
# coding: utf-8

require 'pux/webapi/faceu.rb'

PUX::WebAPI::default_api_key = TEST_API_KEY

class TestWebApiFaceSize < Test::Unit::TestCase

    #
    # 最小検出顔サイズの確認(1)
    # 大→中→小と切り替えて検出数の差を見る
    #
    # @return [void]
    #
    def test_face_size_1
        print( "\n*** #{self.class}::#{__method__}\n")

        #
        # Initialize
        #
        data = data_open( "flickr-0002.jpg")

        #
        # LARGE
        #
        api  = PUX::WebAPI::FaceU.new( :face_size => "LARGE")
        resp = api.detect( data)

        assert_equal( resp.size, 0)


        #
        # MIDDLE
        #
        api  = PUX::WebAPI::FaceU.new( :face_size => "MIDDLE")
        resp = api.detect( data)

        assert_equal( resp.size, 4)


        #
        # SMALL
        #
        api  = PUX::WebAPI::FaceU.new( :face_size => "SMALL")
        resp = api.detect( data)

        assert_equal( resp.size, 7)

    
        #
        # Post process
        #
        data.close
    end

    #
    # 最小検出顔サイズの確認(2)
    # 大→中→小と切り替えて検出数の差を見る
    #
    # @return [void]
    #
    def test_face_size_2
        print( "\n*** #{self.class}::#{__method__}\n")

        #
        # Initialize
        #
        data = data_open( "flickr-0001.jpg")

        #
        # LARGE
        #
        api  = PUX::WebAPI::FaceU.new( :face_size => "LARGE")
        resp = api.detect( data)

        assert_equal( resp.size, 0)


        #
        # MIDDLE
        #
        api  = PUX::WebAPI::FaceU.new( :face_size => "MIDDLE")
        resp = api.detect( data)

        assert_equal( resp.size, 0)


        #
        # SMALL
        #
        api  = PUX::WebAPI::FaceU.new( :face_size => "SMALL")
        resp = api.detect( data)

        assert_equal( resp.size, 13)

    
        #
        # Post process
        #
        data.close
    end
end
