#! /usr/bin/env ruby

require 'test/unit'
require 'pp'

TEST_DIR   = File.dirname( File.expand_path($0))
TEST_FILES = File.join( TEST_DIR, "test_*.rb")

IGNORE_LIST = [
    "test_webapi_key_error.rb"
]


$LOAD_PATH.unshift( File.join( TEST_DIR, "..", "lib"))
$LOAD_PATH.unshift( File.join( TEST_DIR, "..", "test"))

require 'config'

def data_open( file)
    File.open( File.join( TEST_DIR, "data", file), "rb")
end

Dir.glob( TEST_FILES) { |file|
    next if IGNORE_LIST.include?( File.basename( file))
    require file
}
