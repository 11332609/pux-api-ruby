#! /usr/bin/env/ruby
# coding: utf-8

require 'pux/webapi/faceu.rb'

PUX::WebAPI::default_api_key = TEST_API_KEY

class TestWebApiPartsDetect < Test::Unit::TestCase

    # @private
    MASTER_PARTS_TABLE1 = {
        :BOUNDING_RECTANGLE_LEFT_UPPER =>  [605, 146],
        :BOUNDING_RECTANGLE_RIGHT_UPPER =>  [709, 146],
        :BOUNDING_RECTANGLE_RIGHT_LOWER =>  [709, 250],
        :BOUNDING_RECTANGLE_LEFT_LOWER =>  [605, 250],
        :LEFT_BLACK_EYE_CENTER =>  [627, 181],
        :RIGHT_BLACK_EYE_CENTER =>  [676, 176],
        :LEFT_CHEEK1 =>  [603, 185],
        :LEFT_CHEEK2 =>  [604, 202],
        :LEFT_CHEEK3 =>  [607, 218],
        :LEFT_CHEEK4 =>  [611, 232],
        :LEFT_CHEEK5 =>  [620, 246],
        :LEFT_CHEEK6 =>  [631, 256],
        :LEFT_CHEEK7 =>  [644, 263],
        :CHIN =>  [659, 265],
        :RIGHT_CHEEK7 =>  [675, 259],
        :RIGHT_CHEEK6 =>  [687, 250],
        :RIGHT_CHEEK5 =>  [696, 238],
        :RIGHT_CHEEK4 =>  [702, 224],
        :RIGHT_CHEEK3 =>  [705, 210],
        :RIGHT_CHEEK2 =>  [705, 194],
        :RIGHT_CHEEK1 =>  [703, 175],
        :PARIETAL =>  [646, 109],
        :RIGHT_EYEBROW_OUTSIDE_END =>  [693, 165],
        :RIGHT_EYEBROW_UPPER_OUTSIDE =>  [683, 159],
        :RIGHT_EYEBROW_UPPER_INSIDE =>  [670, 162],
        :RIGHT_EYEBROW_INSIDE_END =>  [661, 168],
        :RIGHT_EYEBROW_LOWER_INSIDE =>  [672, 167],
        :RIGHT_EYEBROW_LOWER_OUTSIDE =>  [683, 165],
        :LEFT_EYEBROW_OUTSIDE_END =>  [610, 173],
        :LEFT_EYEBROW_UPPER_OUTSIDE =>  [620, 166],
        :LEFT_EYEBROW_UPPER_INSIDE =>  [633, 167],
        :LEFT_EYEBROW_INSIDE_END =>  [641, 171],
        :LEFT_EYEBROW_LOWER_INSIDE =>  [632, 172],
        :LEFT_EYEBROW_LOWER_OUTSIDE =>  [620, 171],
        :LEFT_EYE_OUTSIDE_END =>  [617, 182],
        :LEFT_EYE_UPPER_OUTSIDE =>  [622, 179],
        :LEFT_EYE_UPPER_CENTER =>  [626, 177],
        :LEFT_EYE_UPPER_INSIDE =>  [631, 178],
        :LEFT_EYE_INSIDE_END =>  [637, 182],
        :LEFT_EYE_LOWER_INSIDE =>  [632, 183],
        :LEFT_EYE_LOWER_CENTER =>  [627, 184],
        :LEFT_EYE_LOWER_OUTSIDE =>  [622, 184],
        :RIGHT_EYE_OUTSIDE_END =>  [686, 175],
        :RIGHT_EYE_UPPER_OUTSIDE =>  [681, 173],
        :RIGHT_EYE_UPPER_CENTER =>  [676, 172],
        :RIGHT_EYE_UPPER_INSIDE =>  [671, 174],
        :RIGHT_EYE_INSIDE_END =>  [666, 179],
        :RIGHT_EYE_LOWER_INSIDE =>  [671, 179],
        :RIGHT_EYE_LOWER_CENTER =>  [677, 179],
        :RIGHT_EYE_LOWER_OUTSIDE =>  [681, 177],
        :NOSE_LEFT_LINE_UPPER =>  [645, 182],
        :NOSE_LEFT_LINE_CENTER =>  [643, 198],
        :NOSE_LEFT_LINE_LOWER0 =>  [636, 210],
        :NOSE_LEFT_LINE_LOWER1 =>  [642, 215],
        :NOSE_BOTTOM_CENTER =>  [654, 217],
        :NOSE_RIGHT_LINE_LOWER1 =>  [667, 212],
        :NOSE_RIGHT_LINE_LOWER0 =>  [671, 206],
        :NOSE_RIGHT_LINE_CENTER =>  [662, 196],
        :NOSE_RIGHT_LINE_UPPER =>  [658, 181],
        :NOSTRILS_LEFT =>  [646, 214],
        :NOSTRILS_RIGHT =>  [662, 212],
        :NOSE_TIP =>  [653, 207],
        :NOSE_CENTER_LINE0 =>  [653, 197],
        :NOSE_CENTER_LINE1 =>  [651, 181],
        :MOUTH_LEFT_END =>  [630, 226],
        :UPPER_LIP_UPPER_LEFT_OUTSIDE =>  [640, 223],
        :UPPER_LIP_UPPER_LEFT_INSIDE =>  [648, 222],
        :MOUTH_UPPER_PART =>  [654, 222],
        :UPPER_LIP_UPPER_RIGHT_INSIDE =>  [660, 220],
        :UPPER_LIP_UPPER_RIGHT_OUTSIDE =>  [668, 220],
        :MOUTH_RIGHT_END =>  [680, 222],
        :LOWER_LIP_LOWER_RIGHT_OUTSIDE =>  [673, 232],
        :LOWER_LIP_LOWER_RIGHT_INSIDE =>  [666, 238],
        :MOUTH_LOWER_PART =>  [656, 241],
        :LOWER_LIP_LOWER_LEFT_INSIDE =>  [645, 240],
        :LOWER_LIP_LOWER_LEFT_OUTSIDE =>  [638, 235],
        :UPPER_LIP_LOWER_LEFT =>  [644, 226],
        :UPPER_LIP_LOWER_CENTER =>  [654, 226],
        :UPPER_LIP_LOWER_RIGHT =>  [665, 224],
        :LOWER_LIP_UPPER_RIGHT =>  [666, 230],
        :LOWER_LIP_UPPER_CENTER =>  [655, 233],
        :LOWER_LIP_UPPER_LEFT =>  [644, 232],
        :MOUTH_CENTER =>  [655, 230]
    }

    #
    # 単純な顔検出の確認
    # パーツ検出付きの顔検出(#1)
    #
    def test_detect_with_parts_1
        print( "\n*** #{self.class}::#{__method__}\n") 

        #
        # Initialize
        #
        api  = PUX::WebAPI::FaceU.new( :parts_detect => true)
        data = data_open( "pakutaso-0001.jpg")
        resp = api.detect( data)

        #
        # Basic parameters
        #
        assert_equal( resp[0].top, 146)
        assert_equal( resp[0].left, 605)
        assert_equal( resp[0].bottom, 250)
        assert_equal( resp[0].right, 709)
        assert_equal( resp[0].left_eye, [627, 181])
        assert_equal( resp[0].right_eye, [676, 176])

        #
        # Parts parameter
        #
        assert( resp[0].parts == MASTER_PARTS_TABLE1)

        #
        # Extended parameters
        #
        assert_nil( resp[0].registered_image)
        assert_nil( resp[0].gender)
        assert_nil( resp[0].smile)
        assert_nil( resp[0].age)
        assert_nil( resp[0].animalize)
        assert_nil( resp[0].smug)
        assert_nil( resp[0].troubled)
        assert_nil( resp[0].face_id)
        assert( !resp[0].has?(:TEST))
        assert_nil( resp[0][:TEST])

        #
        # Post process
        #
        data.close
    end

    # @private
    MASTER_PARTS_TABLE2 = {
        :BOUNDING_RECTANGLE_LEFT_UPPER =>  [592, 165],
        :BOUNDING_RECTANGLE_RIGHT_UPPER =>  [811, 165],
        :BOUNDING_RECTANGLE_RIGHT_LOWER =>  [811, 384],
        :BOUNDING_RECTANGLE_LEFT_LOWER =>  [592, 384],
        :LEFT_BLACK_EYE_CENTER => [653, 224],
        :RIGHT_BLACK_EYE_CENTER => [743, 240],
        :LEFT_CHEEK1 => [605, 214],
        :LEFT_CHEEK2 => [600, 248],
        :LEFT_CHEEK3 => [599, 284],
        :LEFT_CHEEK4 => [603, 322],
        :LEFT_CHEEK5 => [616, 353],
        :LEFT_CHEEK6 => [632, 376],
        :LEFT_CHEEK7 => [651, 395],
        :CHIN => [677, 405],
        :RIGHT_CHEEK7 => [704, 403],
        :RIGHT_CHEEK6 => [728, 390],
        :RIGHT_CHEEK5 => [750, 374],
        :RIGHT_CHEEK4 => [770, 349],
        :RIGHT_CHEEK3 => [785, 317],
        :RIGHT_CHEEK2 => [795, 284],
        :RIGHT_CHEEK1 => [802, 249],
        :PARIETAL => [721, 73],
        :RIGHT_EYEBROW_OUTSIDE_END => [775, 225],
        :RIGHT_EYEBROW_UPPER_OUTSIDE =>  [759, 209],
        :RIGHT_EYEBROW_UPPER_INSIDE => [737, 209],
        :RIGHT_EYEBROW_INSIDE_END => [718, 218],
        :RIGHT_EYEBROW_LOWER_INSIDE => [738, 220],
        :RIGHT_EYEBROW_LOWER_OUTSIDE =>  [757, 221],
        :LEFT_EYEBROW_OUTSIDE_END => [628, 194],
        :LEFT_EYEBROW_UPPER_OUTSIDE => [651, 186],
        :LEFT_EYEBROW_UPPER_INSIDE => [674, 197],
        :LEFT_EYEBROW_INSIDE_END => [688, 212],
        :LEFT_EYEBROW_LOWER_INSIDE => [668, 208],
        :LEFT_EYEBROW_LOWER_OUTSIDE => [648, 199],
        :LEFT_EYE_OUTSIDE_END => [636, 221],
        :LEFT_EYE_UPPER_OUTSIDE => [646, 218],
        :LEFT_EYE_UPPER_CENTER => [655, 218],
        :LEFT_EYE_UPPER_INSIDE => [664, 222],
        :LEFT_EYE_INSIDE_END => [671, 230],
        :LEFT_EYE_LOWER_INSIDE => [661, 231],
        :LEFT_EYE_LOWER_CENTER => [651, 231],
        :LEFT_EYE_LOWER_OUTSIDE => [643, 227],
        :RIGHT_EYE_OUTSIDE_END => [759, 244],
        :RIGHT_EYE_UPPER_OUTSIDE => [752, 237],
        :RIGHT_EYE_UPPER_CENTER => [744, 234],
        :RIGHT_EYE_UPPER_INSIDE => [735, 235],
        :RIGHT_EYE_INSIDE_END => [726, 240],
        :RIGHT_EYE_LOWER_INSIDE => [734, 245],
        :RIGHT_EYE_LOWER_CENTER => [742, 248],
        :RIGHT_EYE_LOWER_OUTSIDE => [750, 247],
        :NOSE_LEFT_LINE_UPPER => [687, 234],
        :NOSE_LEFT_LINE_CENTER => [677, 265],
        :NOSE_LEFT_LINE_LOWER0 => [659, 294],
        :NOSE_LEFT_LINE_LOWER1 => [663, 307],
        :NOSE_BOTTOM_CENTER => [687, 317],
        :NOSE_RIGHT_LINE_LOWER1 => [710, 315],
        :NOSE_RIGHT_LINE_LOWER0 => [718, 304],
        :NOSE_RIGHT_LINE_CENTER => [711, 270],
        :NOSE_RIGHT_LINE_UPPER => [711, 238],
        :NOSTRILS_LEFT => [672, 306],
        :NOSTRILS_RIGHT => [704, 311],
        :NOSE_TIP => [689, 296],
        :NOSE_CENTER_LINE0 => [694, 267],
        :NOSE_CENTER_LINE1 => [699, 236],
        :MOUTH_LEFT_END => [651, 336],
        :UPPER_LIP_UPPER_LEFT_OUTSIDE =>  [664, 331],
        :UPPER_LIP_UPPER_LEFT_INSIDE =>  [677, 331],
        :MOUTH_UPPER_PART => [685, 336],
        :UPPER_LIP_UPPER_RIGHT_INSIDE =>  [694, 334],
        :UPPER_LIP_UPPER_RIGHT_OUTSIDE =>  [706, 339],
        :MOUTH_RIGHT_END => [719, 347],
        :LOWER_LIP_LOWER_RIGHT_OUTSIDE =>  [708, 356],
        :LOWER_LIP_LOWER_RIGHT_INSIDE =>  [697, 362],
        :MOUTH_LOWER_PART => [682, 363],
        :LOWER_LIP_LOWER_LEFT_INSIDE =>  [668, 358],
        :LOWER_LIP_LOWER_LEFT_OUTSIDE =>  [658, 348],
        :UPPER_LIP_LOWER_LEFT => [669, 340],
        :UPPER_LIP_LOWER_CENTER => [684, 344],
        :UPPER_LIP_LOWER_RIGHT => [700, 345],
        :LOWER_LIP_UPPER_RIGHT => [700, 347],
        :LOWER_LIP_UPPER_CENTER => [684, 347],
        :LOWER_LIP_UPPER_LEFT => [669, 341],
        :MOUTH_CENTER => [684, 345],
    }

    #
    # 単純な顔検出の確認
    # パーツ検出付きの顔検出(#2)
    #
    def test_detect_with_parts_2
        print( "\n*** #{self.class}::#{__method__}\n") 

        #
        # Initialize
        #
        api  = PUX::WebAPI::FaceU.new( :parts_detect => true)
        data = data_open( "pakutaso-0002.jpg")
        resp = api.detect( data)

        #
        # Basic parameters
        #
        assert_equal( resp[0].top, 165)
        assert_equal( resp[0].left, 592)
        assert_equal( resp[0].bottom, 384)
        assert_equal( resp[0].right, 811)
        assert_equal( resp[0].left_eye, [653, 224])
        assert_equal( resp[0].right_eye, [743, 240])
        assert_in_delta( resp[0].accuracy, 0.516, 0.001)

        #
        # Parts parameter
        #
        assert( resp[0].parts == MASTER_PARTS_TABLE2)

        #
        # Post process
        #
        data.close
    end
end
