# 素材入手元

## ぱくたそ
[![ぱくたそ](http://www.pakutaso.com/shared/img/site/logo_chara.gif)](http://www.pakutaso.com)

### 利用条件のメモ

 詳細は[こちら](http://www.pakutaso.com/userpolicy.html)

  - クレジット表記が必要になる場合がある。該当するのは以下の場合。
    - 販売物
    - 写真素材を第三者が利用可能な状態で配布する場合。

  二時配布を行う場合は、「ぱくたそ」の利用規約に従う旨をユーザに要求する必要が    ある。

### 利用状況

 |ローカルファイル名|配布元URL| 注記 |
 |-|-|
 | `pakutaso-0001.jpg`| `http://www.pakutaso.com/20150309072post-5277.html` | Sサイズを縮小して利用|
 | `pakutaso-0002.jpg`| `http://www.pakutaso.com/20141015279post-4659.html` | Sサイズを縮小して利用|



## 商用無料の写真検索さん
[![商用無料の写真検索さん](https://nairegift.sakura.ne.jp/freephoto/logo.png){: style="width:35%"}](https://www.nairegift.com/freephoto/)

### 利用条件のメモ
基本的にflickrに登録されている写真のディレクトリサービスなので、実質的にはflickrと同じ。画像毎にライセンスが異なる。

### 利用状況

 |ローカルファイル名|配布元URL| 注記 |
 |-|-|-|
 | `flickr-0001.jpg`| [`https://www.flickr.com/photos/nasacommons/...`](https://www.flickr.com/photos/nasacommons/16478237766/) | NASA on th Commons [[ガイドライン](http://www.nasa.gov/audience/formedia/features/MP_Photo_Guidelines.html#.VRUrXjSsXdI)]|
 | `flickr-0002.jpg`| [`https://www.flickr.com/photos/nasacommons/...`](https://www.flickr.com/photos/nasacommons/15871993874/in/set-72157650350042977) | NASA on th Commons [[ガイドライン](http://www.nasa.gov/audience/formedia/features/MP_Photo_Guidelines.html#.VRUrXjSsXdI)]|

<!-- vi:set ts=2 sw=2 et: -->
