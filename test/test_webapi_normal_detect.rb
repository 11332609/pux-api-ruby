#! /usr/bin/env/ruby
# coding: utf-8

require 'pux/webapi/faceu.rb'

PUX::WebAPI::default_api_key = TEST_API_KEY

class TestWebApiNormalDetect < Test::Unit::TestCase

    #
    # パラメータで渡すAPIキーの評価順序のテスト
    # FaceUクラス生成時に指定された場合は、生成時に指定されてキーを優先する事
    # を確認する
    #
    # @retrrn [void]
    #
    def test_key_priority
        print( "\n*** #{self.class}::#{__method__}\n")

        #
        # Initialize
        #
        PUX::WebAPI::default_api_key = "0" * 32
        data = data_open( "pakutaso-0001.jpg")

        #
        # Test body
        #
        api  = PUX::WebAPI::FaceU.new( :api_key => TEST_API_KEY)
        assert_nothing_raised( RuntimeError) {
            api.detect( data)
        }

        #
        # Post process
        #
        PUX::WebAPI::default_api_key = TEST_API_KEY
        data.close
    end
    
    #
    # 単純な顔検出の確認
    # パラメータなしで呼び出した場合の結果確認
    #
    # @return [void]
    #
    def test_simple_detect
        print( "\n*** #{self.class}::#{__method__}\n")

        #
        # Initialize
        #
        api  = PUX::WebAPI::FaceU.new()
        data = data_open( "pakutaso-0001.jpg")
        resp = api.detect( data)

        #
        # Type check
        #
        assert_equal( resp.size, 1)
        resp.each { |val|
            assert_instance_of( PUX::Person, val)
            assert_kind_of( PUX::WebAPI::PersonForFaceDetect, val)
        }

        #
        # Basic parameters
        #
        assert_equal( resp[0].top, 146)
        assert_equal( resp[0].left, 605)
        assert_equal( resp[0].bottom, 250)
        assert_equal( resp[0].right, 709)
        assert_equal( resp[0].left_eye, [629, 181])
        assert_equal( resp[0].right_eye, [679, 176])

        #
        # Extended parameters
        #
        assert_nil( resp[0].registered_image)
        assert_nil( resp[0].parts)
        assert_nil( resp[0].gender)
        assert_nil( resp[0].smile)
        assert_nil( resp[0].age)
        assert_nil( resp[0].animalize)
        assert_nil( resp[0].smug)
        assert_nil( resp[0].troubled)
        assert_nil( resp[0].face_id)
        assert( !resp[0].has?(:TEST))
        assert_nil( resp[0][:TEST])

        #
        # Post process
        #
        data.close
    end

    #
    # 複数人数の顔検出
    # パラメータなしで呼び出した場合の結果確認
    #
    # @return [void]
    #
    def test_multiple_person_detect_1
        print( "\n*** #{self.class}::#{__method__}\n")

        #
        # Initialize
        #
        api  = PUX::WebAPI::FaceU.new( :face_size => "SMALL")
        data = data_open( "flickr-0002.jpg")
        resp = api.detect( data)

        #
        # Type check
        #
        assert_equal( resp.size, 7)
        resp.each { |val|
            assert_instance_of( PUX::Person, val)
            assert_kind_of( PUX::WebAPI::PersonForFaceDetect, val)
        }

        #
        # Person #1 
        #
        assert_equal( resp[0].top, 157)
        assert_equal( resp[0].left, 77)
        assert_equal( resp[0].bottom, 206)
        assert_equal( resp[0].right, 126)
        assert_equal( resp[0].left_eye, [91, 173])
        assert_equal( resp[0].right_eye, [111, 173])
        assert_in_delta( resp[0].accuracy, 0.564, 0.001)

        #
        # Person #2
        #
        assert_equal( resp[1].top, 225)
        assert_equal( resp[1].left, 339)
        assert_equal( resp[1].bottom, 272)
        assert_equal( resp[1].right, 386)
        assert_equal( resp[1].left_eye, [351, 240])
        assert_equal( resp[1].right_eye, [372, 240])
        assert_in_delta( resp[1].accuracy, 0.530, 0.001)

        #
        # Person #3
        #
        assert_equal( resp[2].top, 141)
        assert_equal( resp[2].left, 225)
        assert_equal( resp[2].bottom, 186)
        assert_equal( resp[2].right, 270)
        assert_equal( resp[2].left_eye, [237, 155])
        assert_equal( resp[2].right_eye, [255, 155])
        assert_in_delta( resp[2].accuracy, 0.371, 0.001)

        #
        # Person #4
        #
        assert_equal( resp[3].top, 232)
        assert_equal( resp[3].left, 512)
        assert_equal( resp[3].bottom, 277)
        assert_equal( resp[3].right, 557)
        assert_equal( resp[3].left_eye, [523, 245])
        assert_equal( resp[3].right_eye, [544, 245])
        assert_in_delta( resp[3].accuracy, 0.330, 0.001)

        #
        # Person #5
        #
        assert_equal( resp[4].top, 234)
        assert_equal( resp[4].left, 161)
        assert_equal( resp[4].bottom, 285)
        assert_equal( resp[4].right, 212)
        assert_equal( resp[4].left_eye, [175, 248])
        assert_equal( resp[4].right_eye, [195, 248])
        assert_in_delta( resp[4].accuracy, 0.317, 0.001)

        #
        # Person #6
        #
        assert_equal( resp[5].top, 114)
        assert_equal( resp[5].left, 370)
        assert_equal( resp[5].bottom, 157)
        assert_equal( resp[5].right, 413)
        assert_equal( resp[5].left_eye, [380, 128])
        assert_equal( resp[5].right_eye, [399, 128])
        assert_in_delta( resp[5].accuracy, 0.349, 0.001)

        #
        # Person #7
        #
        assert_equal( resp[6].top, 141)
        assert_equal( resp[6].left, 501)
        assert_equal( resp[6].bottom, 183)
        assert_equal( resp[6].right, 544)
        assert_equal( resp[6].left_eye, [512, 155])
        assert_equal( resp[6].right_eye, [531, 154])
        assert_in_delta( resp[6].accuracy, 0.444, 0.001)

        #
        # Post proces
        #
        data.close
    end
end
