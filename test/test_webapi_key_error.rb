#! /usr/bin/env/ruby
# coding: utf-8

require 'pux/webapi/faceu.rb'

BAD_API_KEY1 = "0" * 32
BAD_API_KEY2 = "deadbeefcafebabeabadfacefacefeed"

PUX::WebAPI::default_api_key = TEST_API_KEY

class TestWebApiKeyError < Test::Unit::TestCase

    #
    # デフォルトキーにフォーマットの異常なキーを設定した場合の動作の確認
    #
    # @return [void]
    #
    def test_illeagal_key_format_1
        print( "\n*** #{self.class}::#{__method__}\n")

        assert_raise( RuntimeError) {
            PUX::WebAPI::default_api_key = ""
        }

        assert_raise( RuntimeError) {
            PUX::WebAPI::default_api_key = "ABCDEFG"
        }

        assert_raise( RuntimeError) {
            PUX::WebAPI::default_api_key = BAD_API_KEY1 + "0"
        }
    end

    #
    # APIキーにフォーマットの異常なキーを設定した場合の動作の確認
    #
    # @return [void]
    #
    def test_illeagal_key_format_2
        print( "\n*** #{self.class}::#{__method__}\n")

        assert_raise( RuntimeError) {
            PUX::WebAPI::FaceU.new( :api_key => "")
        }

        assert_raise( RuntimeError) {
            PUX::WebAPI::FaceU.new( :api_key => "ABCDEFG")
        }

        assert_raise( RuntimeError) {
            PUX::WebAPI::FaceU.new( :api_key => BAD_API_KEY1 + "0")
        }
    end

    #
    # デフォルトキーに無効なキーを設定した場合の動作の確認
    #
    def test_invalid_api_key_1
        print( "\n*** #{self.class}::#{__method__}\n")

        #
        # Initialize
        #

        # Nothing

        #
        # Test body
        #
        PUX::WebAPI::default_api_key = BAD_API_KEY1

        api  = PUX::WebAPI::FaceU.new()
        data = data_open( "pakutaso-0001.jpg")

        assert_raise( RuntimeError) {
            api.detect( data)
        }

        PUX::WebAPI::default_api_key = BAD_API_KEY2

        api  = PUX::WebAPI::FaceU.new()
        data = data_open( "pakutaso-0001.jpg")

        assert_raise( RuntimeError) {
            api.detect( data)
        }

        #
        # Post process
        #
        PUX::WebAPI::default_api_key = TEST_API_KEY
    end

    #
    # APIキーに無効なキーを設定した場合の動作の確認
    #
    # @return [void]
    #
    def test_invalid_api_key_2
        print( "\n*** #{self.class}::#{__method__}\n")

        #
        # Initialize
        #
        PUX::WebAPI::default_api_key = TEST_API_KEY
        data = data_open( "pakutaso-0001.jpg")

        #
        # test body
        #
        api = PUX::WebAPI::FaceU.new( :api_key => BAD_API_KEY1)

        assert_raise( RuntimeError) {
            api.detect( data)
        }

        api  = PUX::WebAPI::FaceU.new( :api_key => BAD_API_KEY2)

        assert_raise( RuntimeError) {
            api.detect( data)
        }

        #
        # Post process
        #
        data.close
    end
end
